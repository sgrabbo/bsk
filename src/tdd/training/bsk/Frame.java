package tdd.training.bsk;

public class Frame {
	
	protected int firstThrow = 0, secondThrow = 0, score = 0, bonus = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return this.firstThrow;
		
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		
		return this.secondThrow;
		
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		
		this.bonus = this.bonus + bonus;
		
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return this.bonus;
		
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		
		if (this.isSpare() || this.isStrike()) {
			
			this.score = this.firstThrow + this.secondThrow + this.bonus;
			
		} else {
			
			this.score = this.firstThrow + this.secondThrow;
			
		}
		
		return this.score;
		
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {

		if (this.firstThrow == 10) {
			
			this.secondThrow = 0;
			return true;
			
		} else {
			
			return false;
			
		}
		
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		
		if (((this.firstThrow + this.secondThrow) == 10) && (this.firstThrow != 10)) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
	public boolean isEqual(Frame f) {
		
		if ((this.firstThrow == f.getFirstThrow()) && (this.secondThrow == f.getSecondThrow())) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
	}

}
