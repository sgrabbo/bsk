package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	protected ArrayList<Frame> frames;
	protected int score = 0, firstBonusThrow = 0, secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		frames = new ArrayList<Frame> ();
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if (frames.size() <= 9) {
			
			this.frames.add(frame);
			
		} else {
			
			throw new BowlingException("It is not possible to add more than ten frames per game.");
			
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {

		return frames.get(index);	
		
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		this.firstBonusThrow = firstBonusThrow;
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		
		this.secondBonusThrow = secondBonusThrow;
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return this.firstBonusThrow;
		
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		for (int i = 0; i <= 9; i++) {
			
			// If is spare in the last frame
			if (this.frames.get(i).isSpare() && (i == 9)) {
				
				this.frames.get(i).setBonus(this.getFirstBonusThrow());
				
			// If is spare in a frame that is not the last
			} else if (this.frames.get(i).isSpare()) {
				
				this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow());
				
			}
			
			// If is strike in the last frame
			if (this.frames.get(i).isStrike() && (i == 9)) {
				
				this.frames.get(i).setBonus(this.getFirstBonusThrow() + this.getSecondBonusThrow());
				
			// If is strike in the frame 8 and 9
			} else if (this.frames.get(i).isStrike() && this.frames.get(i+1).isStrike() && (i == 8)) {
				
				this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.frames.get(i+1).getSecondThrow() + this.getFirstBonusThrow());
			
			// If is strike in two subsequent frames that are not the frame 8 and the frame 9
			} else if (this.frames.get(i).isStrike() && this.frames.get(i+1).isStrike()) {
				
				this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.frames.get(i+1).getSecondThrow() + this.frames.get(i+2).getFirstThrow());
			
			// If is strike in a single frame (not two subsequent frames) that is not the last	
			} else if (this.frames.get(i).isStrike()) {
				
				this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.frames.get(i+1).getSecondThrow());
				
			}
			
			this.score = this.score + this.frames.get(i).getScore();
			
		}
		
		return this.score;
		
	}

}
