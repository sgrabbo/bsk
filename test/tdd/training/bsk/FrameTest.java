package tdd.training.bsk;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;


public class FrameTest {
	
	Game game;
	
	@Before
	public void setUp() {
		
		game = new Game();
		
	}
	
	@Test
	// Test user story 1
	public void testGetThrows() throws Exception{

		int firstThrow = 2;
		int secondThrow = 4;
		Frame f = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow, f.getFirstThrow());
		assertEquals(secondThrow, f.getSecondThrow());
		
	}
	
	@Test
	// Test user story 2
	public void testGetFrameScore() throws Exception{

		int firstThrow = 2;
		int secondThrow = 6;
		Frame f = new Frame(firstThrow, secondThrow);
		assertEquals(8, f.getScore());
		
	}
	
	@Test
	// Test user story 3
	public void testAddFrame() throws Exception{
		
		ArrayList<Frame> frames = new ArrayList<Frame> ();
		
		Frame f1 = new Frame(1, 5);
		game.addFrame(f1);
		frames.add(f1);
		
		Frame f2 = new Frame(2, 5);
		game.addFrame(f2);
		frames.add(f2);
		
		Frame f3 = new Frame(1, 1);
		game.addFrame(f3);
		frames.add(f3);
		
		Frame f4 = new Frame(4, 2);
		game.addFrame(f4);
		frames.add(f4);
		
		Frame f5 = new Frame(8, 0);
		game.addFrame(f5);
		frames.add(f5);
		
		Frame f6 = new Frame(2, 3);
		game.addFrame(f6);
		frames.add(f6);
		
		Frame f7 = new Frame(1, 3);
		game.addFrame(f7);
		frames.add(f7);
		
		Frame f8 = new Frame(1, 6);
		game.addFrame(f8);
		frames.add(f8);
		
		Frame f9 = new Frame(2, 0);
		game.addFrame(f9);
		frames.add(f9);
		
		Frame f10 = new Frame(10, 0);
		game.addFrame(f10);
		frames.add(f10);
		
		for (int i = 0; i <= 9; i++) {
		
			assertEquals(true, game.getFrameAt(i).isEqual(frames.get(i)));
			
		}
		
	}
	
	@Test
	// Test user story 4
	public void testGetGameScore() throws Exception{
				
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
			
		assertEquals(81, game.calculateScore());
		
	}
	
	@Test
	// Test user story 5
	public void testSpare() throws Exception{
		
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
			
		assertEquals(88, game.calculateScore());
		
	}
	
	@Test
	// Test user story 6
	public void testStrike() throws Exception{
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
			
		assertEquals(94, game.calculateScore());
		
	}
	
	@Test
	// Test user story 7
	public void testStrikeAndSpare() throws Exception{
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
			
		assertEquals(103, game.calculateScore());
		
	}
	
	@Test
	// Test user story 8
	public void testMultipleStrike() throws Exception{
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
		
	}
	
	@Test
	// Test user story 9
	public void testMultipleSpare() throws Exception{
		
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
			
		assertEquals(98, game.calculateScore());
		
	}
	
	@Test
	// Test user story 10
	public void testSpareLastFrame() throws Exception{
				
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
			
		assertEquals(90, game.calculateScore());
		
	}
	
	@Test
	// Test user story 11
	public void testStrikeLastFrame() throws Exception{
				
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
			
		assertEquals(92, game.calculateScore());
		
	}
	
	@Test
	// Test user story 12
	public void testPerfectGame() throws Exception{
				
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
			
		assertEquals(300, game.calculateScore());
		
	}
	
	// User Story 13
	// A single game consists of 10 frames.
	// Requirement: Implement Game.addFrame(Frame frame) to throw a BowlingException if more than ten
	// frames are added to a single game.	
	@Test (expected = BowlingException.class)
	// Test user story 13
	public void testAddingMoreThanTenFrame() throws Exception{
				
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.addFrame(new Frame(4, 6)); // eleventh game.
		
	}

}
